console.log("== Test Logika dan Algoritma ==");

console.log("== soal 1 ==");
var inputan = "SELamAT PaGi Dunia!!";
var hapusTandabaca = inputan.replaceAll(/[^ A-Za-z0-9]/g, "");
var hurufKecil = hapusTandabaca.toLowerCase();
var result2 = hurufKecil.replaceAll(" ", "-");
var soal1 = hurufKecil.split(" ");
for (let i = 0; i < soal1.length; i++) {
  soal1[i] = soal1[i][0].toUpperCase() + soal1[i].substr(1);
}
result1 = soal1.join(" ");
console.log(result1);
console.log(result2);

console.log("== soal 2 ==");
var input = "aabbbahwws";
var split = input.split("");
const jumlahnya = split.reduce((curr, element, idx) => {
  curr[element] = (curr[element] ?? 0) + 1;
  return { ...curr };
}, {});
Object.keys(jumlahnya).forEach((el) => console.log(el + " = " + jumlahnya[el]));

console.log("== soal 3 ==");
var input = 10;
var ganjil = 1;
var arr1 = [];
var arr2 = [1];
var arr3 = [0, 0];
for (var angka = 1; angka <= input; angka++) {
  arr1.push(angka * angka);
}
for (var angka = 0; angka < input - 1; angka++) {
  arr2.push(arr2[angka] + ganjil);
  ganjil += 2;
}
for (var angka = 0; angka < input - 2; angka++) {
  arr3.push(arr3[angka] + arr3[angka + 1] + 1);
}
console.log(arr1.join(" "));
console.log(arr2.join(" "));
console.log(arr3.join(" "));

console.log("== soal 4 ==");
var masukan = "20,21,80 21 55 31 22";
var masukanArr = masukan.split(/[ ,]/g);

var total = masukanArr.reduce((x, y) => {
  return Number(x) + Number(y);
}, 0);
console.log;
console.log("nilai terkecil :", masukanArr.sort()[0]);
console.log("nilai terbesar :", masukanArr.sort().reverse()[0]);
console.log("nilai rata-rata :", (total / masukanArr.length).toFixed(3));

